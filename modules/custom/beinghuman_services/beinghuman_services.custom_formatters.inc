<?php
/**
 * @file
 * beinghuman_services.custom_formatters.inc
 */

/**
 * Implements hook_custom_formatters_defaults().
 */
function beinghuman_services_custom_formatters_defaults() {
  $export = array();

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'address_city';
  $formatter->label = 'Address: City';
  $formatter->description = '';
  $formatter->mode = 'php';
  $formatter->field_types = 'addressfield';
  $formatter->code = 'return $items[0][\'locality\'];';
  $formatter->fapi = '';
  $export['address_city'] = $formatter;

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'city_name';
  $formatter->label = 'City Name';
  $formatter->description = '';
  $formatter->mode = 'php';
  $formatter->field_types = 'taxonomy_term_reference';
  $formatter->code = 'if (!empty($variables[\'#object\']->field_event_location[LANGUAGE_NONE][0][\'tid\'])) {
  $term = taxonomy_term_load($variables[\'#object\']->field_event_location[LANGUAGE_NONE][0][\'tid\']);
  $city = explode(":", $term->name);
  echo ucfirst(strtolower($city[0]));
}';
  $formatter->fapi = '';
  $export['city_name'] = $formatter;

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'program_brochure';
  $formatter->label = 'Program Brochure';
  $formatter->description = '';
  $formatter->mode = 'php';
  $formatter->field_types = 'file';
  $formatter->code = 'if (!empty($variables[\'#object\']->field_service_pdf[LANGUAGE_NONE][0][\'uri\'])) {
  $url = file_create_url($variables[\'#object\']->field_service_pdf[LANGUAGE_NONE][0][\'uri\']);
  echo "<div class=\'pdf-container\'><a class=\'pdf\' href=" . $url . ">Download Program Brochure</a> <span class=\'adobe\'><a href=\'http://get.adobe.com/uk/reader/\'>Adobe Reader</a> is required</span></div>";
}';
  $formatter->fapi = '';
  $export['program_brochure'] = $formatter;

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'register_or_contact_button';
  $formatter->label = 'Register or Contact Button';
  $formatter->description = '';
  $formatter->mode = 'php';
  $formatter->field_types = 'taxonomy_term_reference';
  $formatter->code = '$node = $variables[\'#object\'];

if (isset($node->field_service_category)) {
  $term = taxonomy_term_load($node->field_service_category[LANGUAGE_NONE][0][\'tid\']);
  switch ($term->field_category_type[LANGUAGE_NONE][0][\'value\']) {
    case \'inhouse\':
      return "<a class=\'service-detail-register-button\' href=\'/contact-us\'>Contact Us</a>";

    case \'free\':
      return "<a class=\'service-detail-register-button\' href=\'/webinar-registration?id=" . $variables[\'#object\']->nid . "\'>Register Now</a>";

    default:
      return "<a class=\'service-detail-register-button\' href=\'/program-registration?id=" . $variables[\'#object\']->nid . "\'>Register Now</a>";
  }
}';
  $formatter->fapi = '';
  $export['register_or_contact_button'] = $formatter;

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'registration_fee';
  $formatter->label = 'Registration Fee';
  $formatter->description = '';
  $formatter->mode = 'php';
  $formatter->field_types = 'text_long';
  $formatter->code = '$items = array(
  \'field_service_cost_early_bird\' => t(\'Early bird\'),
  \'field_service_cost_single\' => t(\'Single delegate\'),
  \'field_service_cost_team\' => t(\'Project Team Discount\'),
  \'field_service_cost_cmi_acmp\' => t(\'CMI/ACMP Discount\'),
);
foreach ($items as $field_name => $title) {
  if (isset($variables[\'#object\']->{$field_name}[LANGUAGE_NONE][0][\'value\'])) {
    echo"<span class=\'registration-fee-title\'>" . $title . ":</span> <strong> AUD $" . str_replace(".00", "", $variables[\'#object\']->{$field_name}[LANGUAGE_NONE][0][\'value\']) . "</strong><br>";
  }
}

if (!empty($variables[\'#object\']->field_service_cost_comments[LANGUAGE_NONE][0][\'value\'])) {
  echo $variables[\'#object\']->field_service_cost_comments[LANGUAGE_NONE][0][\'value\'] . "<br>";
}';
  $formatter->fapi = '';
  $export['registration_fee'] = $formatter;

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'service_date';
  $formatter->label = 'Service Date';
  $formatter->description = '';
  $formatter->mode = 'php';
  $formatter->field_types = 'datetime';
  $formatter->code = 'if (date("d M Y", strtotime($variables[\'#items\'][0][\'value\'])) == date("d M Y", strtotime($variables[\'#items\'][0][\'value2\']))) {
	echo date("d M Y", strtotime($variables[\'#items\'][0][\'value\']));
}
else {
  if (date("M Y", strtotime($variables[\'#items\'][0][\'value\'])) == date("M Y", strtotime($variables[\'#items\'][0][\'value2\']))) {
  	echo date("d", strtotime($variables[\'#items\'][0][\'value\'])) . "-" . date("d M Y", strtotime($variables[\'#items\'][0][\'value2\']));
  }
  else {
    echo date("d M", strtotime($variables[\'#items\'][0][\'value\'])) . " - " . date("d M Y", strtotime($variables[\'#items\'][0][\'value2\']));
  }
}
';
  $formatter->fapi = '';
  $export['service_date'] = $formatter;

  return $export;
}
