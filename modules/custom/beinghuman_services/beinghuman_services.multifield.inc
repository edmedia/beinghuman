<?php
/**
 * @file
 * beinghuman_services.multifield.inc
 */

/**
 * Implements hook_multifield_default_multifield().
 */
function beinghuman_services_multifield_default_multifield() {
  $export = array();

  $multifield = new stdClass();
  $multifield->disabled = FALSE; /* Edit this to true to make a default multifield disabled initially */
  $multifield->api_version = 1;
  $multifield->machine_name = 'tab';
  $multifield->label = 'Tab';
  $multifield->description = '';
  $export['tab'] = $multifield;

  return $export;
}
