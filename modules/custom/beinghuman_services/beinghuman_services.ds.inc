<?php
/**
 * @file
 * beinghuman_services.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function beinghuman_services_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|event|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'event';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'field_event_date' => array(
      'formatter_settings' => array(
        'ft' => array(),
      ),
    ),
    'field_event_topic_title' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'fis' => TRUE,
          'fis-el' => 'h2',
          'fis-cl' => '',
          'fis-at' => '',
          'fis-def-at' => FALSE,
        ),
      ),
    ),
  );
  $export['node|event|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|service|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'service';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'dates' => array(
      'weight' => '25',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'past_dates' => array(
      'weight' => '30',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'upcoming_dates' => array(
      'weight' => '0',
      'label' => 'above',
      'format' => 'default',
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'lb' => ' dates & locations',
          'lb-el' => 'h2',
          'lb-col' => TRUE,
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'location',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
        ),
      ),
    ),
    'field_service_benefits' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'lb-el' => 'h2',
          'lb-col' => TRUE,
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'benefits',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
          'fis' => TRUE,
          'fis-el' => 'ul',
          'fis-cl' => '',
          'fis-at' => '',
          'fis-def-at' => FALSE,
          'fi' => TRUE,
          'fi-el' => 'li',
          'fi-cl' => '',
          'fi-at' => '',
          'fi-def-at' => FALSE,
          'fi-odd-even' => FALSE,
        ),
      ),
    ),
    'field_service_cost_comments' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'lb' => 'Registration Fee',
          'lb-el' => 'h2',
          'lb-col' => TRUE,
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'fee',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
        ),
      ),
    ),
    'field_service_duration' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'lb-el' => 'h2',
          'lb-col' => TRUE,
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'duration',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
        ),
      ),
    ),
    'field_service_inclusions' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'lb-el' => 'h2',
          'lb-col' => TRUE,
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'inclusions',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
          'fis' => TRUE,
          'fis-el' => 'ul',
          'fis-cl' => '',
          'fis-at' => '',
          'fis-def-at' => FALSE,
          'fi' => TRUE,
          'fi-el' => 'li',
          'fi-cl' => '',
          'fi-at' => '',
          'fi-def-at' => FALSE,
          'fi-odd-even' => FALSE,
        ),
      ),
    ),
    'field_service_who' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'lb-el' => 'h2',
          'lb-col' => TRUE,
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'attend',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
          'fis' => TRUE,
          'fis-el' => 'ul',
          'fis-cl' => '',
          'fis-at' => '',
          'fis-def-at' => FALSE,
          'fi' => TRUE,
          'fi-el' => 'li',
          'fi-cl' => '',
          'fi-at' => '',
          'fi-def-at' => FALSE,
          'fi-odd-even' => FALSE,
        ),
      ),
    ),
    'field_presenters' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'lb' => 'presenters',
          'lb-el' => 'h2',
          'lb-col' => TRUE,
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'presenters',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
        ),
      ),
    ),
  );
  $export['node|service|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'taxonomy_term|service_location|full';
  $ds_fieldsetting->entity_type = 'taxonomy_term';
  $ds_fieldsetting->bundle = 'service_location';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['taxonomy_term|service_location|full'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function beinghuman_services_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'dates';
  $ds_field->label = 'Dates';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|service_dates-block',
    'block_render' => '1',
  );
  $export['dates'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'past_dates';
  $ds_field->label = 'Past dates';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|service_dates-block_1',
    'block_render' => '1',
  );
  $export['past_dates'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'upcoming_dates';
  $ds_field->label = 'Upcoming dates';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|service_dates-block_2',
    'block_render' => '3',
  );
  $export['upcoming_dates'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function beinghuman_services_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|event|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'event';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'field_event_service',
      ),
      'left' => array(
        1 => 'field_event_date',
      ),
      'right' => array(
        2 => 'field_event_status',
        3 => 'field_event_staff',
        4 => 'field_event_mp3',
      ),
      'footer' => array(
        5 => 'field_event_location',
        6 => 'field_event_topic_title',
        7 => 'field_event_topic_description',
      ),
    ),
    'fields' => array(
      'field_event_service' => 'header',
      'field_event_date' => 'left',
      'field_event_status' => 'right',
      'field_event_staff' => 'right',
      'field_event_mp3' => 'right',
      'field_event_location' => 'footer',
      'field_event_topic_title' => 'footer',
      'field_event_topic_description' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|event|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|service|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'service';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'upcoming_dates',
        1 => 'field_service_duration',
        2 => 'field_presenters',
        3 => 'field_service_cost_comments',
      ),
      'right' => array(
        4 => 'field_service_who',
        5 => 'field_service_benefits',
        6 => 'field_service_inclusions',
        7 => 'field_service_category',
      ),
      'footer' => array(
        8 => 'body',
        9 => 'field_service_pdf',
        10 => 'group_tabs',
        11 => 'field_service_overview',
        12 => 'field_service_agenda',
        13 => 'field_service_prework',
        14 => 'group_overview',
        15 => 'dates',
        16 => 'group_dates',
        17 => 'group_agenda',
        18 => 'group_prework',
        19 => 'group_past_dates',
        20 => 'past_dates',
      ),
    ),
    'fields' => array(
      'upcoming_dates' => 'left',
      'field_service_duration' => 'left',
      'field_presenters' => 'left',
      'field_service_cost_comments' => 'left',
      'field_service_who' => 'right',
      'field_service_benefits' => 'right',
      'field_service_inclusions' => 'right',
      'field_service_category' => 'right',
      'body' => 'footer',
      'field_service_pdf' => 'footer',
      'group_tabs' => 'footer',
      'field_service_overview' => 'footer',
      'field_service_agenda' => 'footer',
      'field_service_prework' => 'footer',
      'group_overview' => 'footer',
      'dates' => 'footer',
      'group_dates' => 'footer',
      'group_agenda' => 'footer',
      'group_prework' => 'footer',
      'group_past_dates' => 'footer',
      'past_dates' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|service|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|service|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'service';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'title',
      ),
      'left' => array(
        1 => 'group_info_duration',
        2 => 'group_info',
        3 => 'group_presenters',
        5 => 'field_presenters',
        8 => 'group_service_cost',
        11 => 'group_info_who',
        12 => 'group_info_benefits',
        15 => 'group_service_info_inclusions',
        23 => 'field_service_benefits',
        24 => 'field_service_who',
        25 => 'field_service_inclusions',
        29 => 'field_service_duration',
        30 => 'field_service_cost_early_bird',
        31 => 'field_service_cost_single',
        32 => 'field_service_cost_team',
        33 => 'field_service_cost_cmi_acmp',
        34 => 'field_service_cost_comments',
      ),
      'right' => array(
        4 => 'field_service_salesforce_code',
        7 => 'field_service_category',
        9 => 'field_service_roles',
      ),
      'footer' => array(
        6 => 'group_tabs_overview',
        10 => 'group_tabs_date',
        13 => 'body',
        14 => 'group_tabs_agenda',
        16 => 'group_tabs_prework',
        17 => 'field_service_pdf',
        18 => 'group_tabs',
        19 => 'field_service_tabs',
        20 => 'redirect',
        21 => 'path',
        22 => 'metatags',
        26 => 'field_service_overview',
        27 => 'field_service_agenda',
        28 => 'field_service_prework',
      ),
      'hidden' => array(
        35 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'title' => 'header',
      'group_info_duration' => 'left',
      'group_info' => 'left',
      'group_presenters' => 'left',
      'field_service_salesforce_code' => 'right',
      'field_presenters' => 'left',
      'group_tabs_overview' => 'footer',
      'field_service_category' => 'right',
      'group_service_cost' => 'left',
      'field_service_roles' => 'right',
      'group_tabs_date' => 'footer',
      'group_info_who' => 'left',
      'group_info_benefits' => 'left',
      'body' => 'footer',
      'group_tabs_agenda' => 'footer',
      'group_service_info_inclusions' => 'left',
      'group_tabs_prework' => 'footer',
      'field_service_pdf' => 'footer',
      'group_tabs' => 'footer',
      'field_service_tabs' => 'footer',
      'redirect' => 'footer',
      'path' => 'footer',
      'metatags' => 'footer',
      'field_service_benefits' => 'left',
      'field_service_who' => 'left',
      'field_service_inclusions' => 'left',
      'field_service_overview' => 'footer',
      'field_service_agenda' => 'footer',
      'field_service_prework' => 'footer',
      'field_service_duration' => 'left',
      'field_service_cost_early_bird' => 'left',
      'field_service_cost_single' => 'left',
      'field_service_cost_team' => 'left',
      'field_service_cost_cmi_acmp' => 'left',
      'field_service_cost_comments' => 'left',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|service|form'] = $ds_layout;

  return $export;
}
