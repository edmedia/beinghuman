<?php
/**
 * @file
 * beinghuman_services.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function beinghuman_services_taxonomy_default_vocabularies() {
  return array(
    'service_categories' => array(
      'name' => 'Service categories',
      'machine_name' => 'service_categories',
      'description' => 'Service categories',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'service_location' => array(
      'name' => 'Service location',
      'machine_name' => 'service_location',
      'description' => 'Service location',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'service_roles' => array(
      'name' => 'Service roles',
      'machine_name' => 'service_roles',
      'description' => 'Service roles',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'service_topic' => array(
      'name' => 'Service topic',
      'machine_name' => 'service_topic',
      'description' => 'Service topic',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
