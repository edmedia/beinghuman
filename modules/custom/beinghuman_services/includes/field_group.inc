<?php
/**
 * @file
 * Fieldgroup module integration.
 */

/**
 * Implements hook_field_group_build_pre_render_alter().
 */
function beinghuman_services_field_group_build_pre_render_alter(&$element) {
  if (!isset($element['#node_edit_form']) && isset($element['group_tabs']) && isset($element['#node'])) {
    // Ensure that empty tabs don't show up.
    foreach (element_children($element['group_tabs']) as $tab) {
      foreach (element_children($element['group_tabs'][$tab]) as $field_name) {
        if (empty($element['group_tabs'][$tab][$field_name]['#items'][0]['value'])) {
          unset($element['group_tabs'][$tab]);
        }
      }
    }

    // Inject additional tabs into the Fieldgroup.
    foreach ($element['field_service_tabs']['#items'] as $delta => $tab) {
      if (!empty($tab['field_tab_body'][LANGUAGE_NONE][0]['value']) && !empty($tab['field_tab_title'][LANGUAGE_NONE][0]['value'])) {
        $last_child = end(element_children($element['group_tabs']));
        $weight = !empty($last_child) ? $element['group_tabs'][$last_child]['#weight'] : 0;
        $element['group_tabs']["service_tab_{$delta}"] = array(
          '#id' => "service_tab_{$delta}",
          '#type' => 'fieldset',
          '#weight' => $weight + 1,
          '#title' => $tab['field_tab_title'][LANGUAGE_NONE][0]['value'],
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
          '#group' => 'group_tabs',
          '#parents' => array('group_tabs'),
          '#description' => '',
        );
        $element['group_tabs']["service_tab_{$delta}"]['body'] = array(
          '#markup' => $tab['field_tab_body'][LANGUAGE_NONE][0]['safe_value'],
        );
      }
    }
  }
}
