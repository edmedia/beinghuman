<?php
/**
 * @file
 * beinghuman_core.custom_formatters.inc
 */

/**
 * Implements hook_custom_formatters_defaults().
 */
function beinghuman_core_custom_formatters_defaults() {
  $export = array();

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'multifield_prices';
  $formatter->label = 'Multifield: Prices';
  $formatter->description = '';
  $formatter->mode = 'php';
  $formatter->field_types = 'prices';
  $formatter->code = '$element = array();
$prices = beinghuman_core_get_prices($variables[\'#object\']);

foreach ($prices as $delta => $price) {
  $discount = \'\';
  if ($delta > 0) {
    $discount = t(\'(@percentage% saving)\', array(\'@percentage\' => round(100 - $price[\'price\'] / $prices[0][\'price\'] * 100)));
  }

  $element[] = array(
    \'#type\' => \'item\',
    \'#title\' => t(\'@range copies (inc. GST)\', array(\'@range\' => $price[\'label\'])),
    \'#markup\' => t(\'AU $@price @discount\', array(
      \'@price\' => $price[\'price\'],
      \'@discount\' => $discount,
    )),
  );
}

return $element;';
  $formatter->fapi = '';
  $export['multifield_prices'] = $formatter;

  return $export;
}
