<?php
/**
 * @file
 * beinghuman_core.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function beinghuman_core_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'content-about_us-our_team';
  $context->description = '';
  $context->tag = 'beinghuman::content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'about-us/our-team' => 'about-us/our-team',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-staff-block' => array(
          'module' => 'views',
          'delta' => 'staff-block',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('beinghuman::content');
  $export['content-about_us-our_team'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'layout-blog';
  $context->description = '';
  $context->tag = 'beinghuman::layout';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'blog*' => 'blog*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-latest_blog-block' => array(
          'module' => 'views',
          'delta' => 'latest_blog-block',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'views-archive-block' => array(
          'module' => 'views',
          'delta' => 'archive-block',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('beinghuman::layout');
  $export['layout-blog'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'layout-bookstore';
  $context->description = '';
  $context->tag = 'beinghuman::layout';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'bookstore*' => 'bookstore*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-catalog-block' => array(
          'module' => 'views',
          'delta' => 'catalog-block',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'basic_cart-shopping_cart' => array(
          'module' => 'basic_cart',
          'delta' => 'shopping_cart',
          'region' => 'sidebar_second',
          'weight' => '-25',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('beinghuman::layout');
  $export['layout-bookstore'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'layout-checkout';
  $context->description = '';
  $context->tag = 'beinghuman::layout';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'checkout' => 'checkout',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'basic_cart-shopping_cart' => array(
          'module' => 'basic_cart',
          'delta' => 'shopping_cart',
          'region' => 'content',
          'weight' => '-25',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('beinghuman::layout');
  $export['layout-checkout'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'layout-contact_us';
  $context->description = '';
  $context->tag = 'beinghuman::layout';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'contact-us' => 'contact-us',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'bean-the-guest-house' => array(
          'module' => 'bean',
          'delta' => 'the-guest-house',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('beinghuman::layout');
  $export['layout-contact_us'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'layout-front_not';
  $context->description = '';
  $context->tag = 'beinghuman::layout';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~<front>' => '~<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'panels_mini-upcoming_events' => array(
          'module' => 'panels_mini',
          'delta' => 'upcoming_events',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'bean-heartplace-scholarships' => array(
          'module' => 'bean',
          'delta' => 'heartplace-scholarships',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('beinghuman::layout');
  $export['layout-front_not'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'layout-global';
  $context->description = '';
  $context->tag = 'beinghuman::layout';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu_block-main_menu' => array(
          'module' => 'menu_block',
          'delta' => 'main_menu',
          'region' => 'navigation',
          'weight' => '-10',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'panels_mini-footer' => array(
          'module' => 'panels_mini',
          'delta' => 'footer',
          'region' => 'footer',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('beinghuman::layout');
  $export['layout-global'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'layout-not_front_or_blog_or_services';
  $context->description = '';
  $context->tag = 'beinghuman::layout';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~<front>' => '~<front>',
        '~blog*' => '~blog*',
        '~services*' => '~services*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu_block-left_sidebar_menu' => array(
          'module' => 'menu_block',
          'delta' => 'left_sidebar_menu',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('beinghuman::layout');
  $export['layout-not_front_or_blog_or_services'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'server-development';
  $context->description = '';
  $context->tag = 'beinghuman_core::server';
  $context->conditions = array(
    'server_vars' => array(
      'values' => array(
        'variables' => array(
          0 => array(
            'name' => 'HTTP_HOST',
            'op' => 'strpos',
            'negate' => 1,
            'value' => 'beinghuman.com.au',
          ),
        ),
        'and_or' => 0,
      ),
    ),
  );
  $context->reactions = array(
    'module' => array(
      'coder' => 'enable',
      'coder_review' => 'enable',
      'coffee' => 'enable',
      'context_ui' => 'enable',
      'devel' => 'enable',
      'dblog' => 'enable',
      'diff' => 'enable',
      'ds_ui' => 'enable',
      'entitycache' => 'disable',
      'field_ui' => 'enable',
      'module_filter' => 'enable',
      'overlay_expand' => 'enable',
      'reroute_email' => 'enable',
      'search_krumo' => 'enable',
      'styleguide' => 'enable',
      'update' => 'enable',
      'variable_admin' => 'enable',
      'views_ui' => 'enable',
    ),
    'variable' => array(
      '#set' => array(
        'environment_indicator_overwrite' => 0,
        'environment_indicator_overwritten_color' => 0,
        'environment_indicator_overwritten_name' => 0,
        'preprocess_css' => 0,
        'preprocess_js' => 0,
        'reroute_email_address' => 0,
        'reroute_email_enable' => 0,
      ),
      'environment_indicator_overwrite' => 1,
      'environment_indicator_overwritten_color' => '#FF0000',
      'environment_indicator_overwritten_name' => 'Development',
      'googleanalytics_account' => 'UA-0000000-0',
      'preprocess_css' => 0,
      'preprocess_js' => 0,
      'reroute_email_address' => 'developers@realityloop.com',
      'reroute_email_enable' => 1,
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('beinghuman_core::server');
  $export['server-development'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'server-production';
  $context->description = '';
  $context->tag = 'beinghuman_core::server';
  $context->conditions = array(
    'server_vars' => array(
      'values' => array(
        'variables' => array(
          0 => array(
            'name' => 'HTTP_HOST',
            'op' => '==',
            'negate' => 0,
            'value' => 'beinghuman.com.au',
          ),
        ),
        'and_or' => 0,
      ),
    ),
  );
  $context->reactions = array(
    'module' => array(
      'entitycache' => 'enable',
      'coder' => 'disable',
      'coder_review' => 'disable',
      'coffee' => 'disable',
      'context_ui' => 'disable',
      'devel' => 'disable',
      'dblog' => 'disable',
      'diff' => 'disable',
      'ds_ui' => 'disable',
      'field_ui' => 'disable',
      'module_filter' => 'disable',
      'overlay_expand' => 'disable',
      'reroute_email' => 'disable',
      'search_krumo' => 'disable',
      'styleguide' => 'disable',
      'update' => 'disable',
      'views_ui' => 'disable',
      'variable_admin' => 'disable',
    ),
    'variable' => array(
      '#set' => array(
        'environment_indicator_suppress_pages' => 0,
      ),
      'environment_indicator_suppress_pages' => '*',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('beinghuman_core::server');
  $export['server-production'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'server-staging';
  $context->description = '';
  $context->tag = 'beinghuman_core::server';
  $context->conditions = array(
    'server_vars' => array(
      'values' => array(
        'variables' => array(
          0 => array(
            'name' => 'HTTP_HOST',
            'op' => 'preg_match',
            'negate' => 0,
            'value' => '/^stag/',
          ),
        ),
        'and_or' => 0,
      ),
    ),
  );
  $context->reactions = array(
    'module' => array(
      'coder' => 'disable',
      'coder_review' => 'disable',
      'coffee' => 'disable',
      'context_ui' => 'disable',
      'devel' => 'disable',
      'dblog' => 'disable',
      'diff' => 'disable',
      'ds_ui' => 'disable',
      'entitycache' => 'enable',
      'field_ui' => 'disable',
      'module_filter' => 'disable',
      'overlay_expand' => 'disable',
      'search_krumo' => 'disable',
      'styleguide' => 'disable',
      'update' => 'disable',
      'views_ui' => 'disable',
      'variable_admin' => 'disable',
      'reroute_email' => 'enable',
    ),
    'variable' => array(
      '#set' => array(
        'environment_indicator_overwrite' => 0,
        'environment_indicator_overwritten_color' => 0,
        'environment_indicator_overwritten_name' => 0,
      ),
      'environment_indicator_overwrite' => 1,
      'environment_indicator_overwritten_color' => '#000000',
      'environment_indicator_overwritten_name' => 'Staging',
      'googleanalytics_account' => 'UA-0000000-0',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('beinghuman_core::server');
  $export['server-staging'] = $context;

  return $export;
}
