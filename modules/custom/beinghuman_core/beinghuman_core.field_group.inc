<?php
/**
 * @file
 * beinghuman_core.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function beinghuman_core_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_billing|entityform|program_registration|form';
  $field_group->group_name = 'group_billing';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'program_registration';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Billing Details for Tax Invoice/Receipt',
    'weight' => '4',
    'children' => array(
      0 => 'field_billing_address',
      1 => 'field_billing_details',
      2 => 'field_billing_name',
      3 => 'field_billling_organisation',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-billing field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_billing|entityform|program_registration|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_participant2|entityform|program_registration|form';
  $field_group->group_name = 'group_participant2';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'program_registration';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '2nd Participant',
    'weight' => '1',
    'children' => array(
      0 => 'field_p2_email_address',
      1 => 'field_p2_emergency_contact_name',
      2 => 'field_p2_emergency_contact_numbe',
      3 => 'field_p2_first_name',
      4 => 'field_p2_job_title',
      5 => 'field_p2_last_name',
      6 => 'field_p2_phone_number',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_participant2|entityform|program_registration|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_participant3|entityform|program_registration|form';
  $field_group->group_name = 'group_participant3';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'program_registration';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '3rd Participant',
    'weight' => '2',
    'children' => array(
      0 => 'field_p3_email_address',
      1 => 'field_p3_emergency_contact_name',
      2 => 'field_p3_emergency_contact_numbe',
      3 => 'field_p3_first_name',
      4 => 'field_p3_job_title',
      5 => 'field_p3_last_name',
      6 => 'field_p3_phone_number',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_participant3|entityform|program_registration|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_payment|entityform|checkout|form';
  $field_group->group_name = 'group_payment';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'checkout';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Payment Method',
    'weight' => '7',
    'children' => array(
      0 => 'field_3_or_4_digit_ccv',
      1 => 'field_billing_address',
      2 => 'field_billing_details',
      3 => 'field_billing_name',
      4 => 'field_billling_organisation',
      5 => 'field_card_expiry',
      6 => 'field_card_number',
      7 => 'field_card_type',
      8 => 'field_cardholders_name',
      9 => 'field_payment_method',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-payment field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_payment|entityform|checkout|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_payment|entityform|program_registration|form';
  $field_group->group_name = 'group_payment';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'program_registration';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Payment Method',
    'weight' => '3',
    'children' => array(
      0 => 'field_3_or_4_digit_ccv',
      1 => 'field_card_expiry',
      2 => 'field_card_number',
      3 => 'field_card_type',
      4 => 'field_cardholders_name',
      5 => 'field_payment_method',
      6 => 'field_promotional_code',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-payment field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_payment|entityform|program_registration|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_registration|entityform|program_registration|form';
  $field_group->group_name = 'group_registration';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'program_registration';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Registration Form',
    'weight' => '0',
    'children' => array(
      0 => 'field_additional_information',
      1 => 'field_contact_address',
      2 => 'field_cost',
      3 => 'field_dietary_requirements',
      4 => 'field_email_address',
      5 => 'field_emergency_contact_name',
      6 => 'field_emergency_contact_number',
      7 => 'field_event_location_public',
      8 => 'field_event_type_public',
      9 => 'field_first_name',
      10 => 'field_job_title',
      11 => 'field_last_name',
      12 => 'field_number_of_participants',
      13 => 'field_organisation',
      14 => 'field_phone',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-registration field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_registration|entityform|program_registration|form'] = $field_group;

  return $export;
}
