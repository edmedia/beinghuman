<?php
/**
 * @file
 * Address Field module integration.
 */

/**
 * Implements hook_field_widget_form_alter().
 */
function beinghuman_core_field_widget_form_alter(&$element, &$form_state, $context) {
  if ('addressfield' == $context['field']['type']) {
    $element['street_block']['premise']['#access'] = FALSE;

    if ('free_events_registration' == $element['#bundle']) {
      $element['street_block']['thoroughfare']['#required'] = FALSE;
      $element['locality_block']['postal_code']['#required'] = FALSE;
    }
  }
}
