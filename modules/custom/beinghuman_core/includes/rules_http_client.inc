<?php
/**
 * @file
 * Rules HTTP Client module integration.
 */

/**
 * Implements hook_rules_http_client_request_alter().
 */
function beinghuman_core_rules_http_client_request_alter(&$url, &$options) {
  // Set field ID by salesforce code.
  $replacement = '00ND0000004DOl3';
  if (strstr($options['data'], '00N20000001iJkb=teleconference')) {
    $replacement = '00ND00000052bqy';
  }
  elseif (strstr($options['data'], '00N20000001iJkb=webinar')) {
    $replacement = '00ND0000005b6xo';
  }
  elseif (strstr($options['data'], '00N20000001iJkb=seminar')) {
    $replacement = '00ND0000002oPKh';
  }

  $options['data'] = preg_replace('/00ND0000004DOl3\[\d+\]/', $replacement, $options['data']);
  $options['data'] = str_replace('Prosci%20Graduate%20Teleconferences%20-%20', '', $options['data']);
  $options['data'] = str_replace('Prosci%20Community%20of%20Practice%20Teleconference%20-%20', '', $options['data']);
  $options['data'] = str_replace('Change%20Conversations%20-%20', '', $options['data']);
  $options['data'] = str_replace('Prosci%20Webinars%20-%20', '', $options['data']);
}
