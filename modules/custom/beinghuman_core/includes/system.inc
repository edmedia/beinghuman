<?php
/**
 * @file
 * System module integration.
 */

/**
 * Implements hook_form_alter().
 */
function beinghuman_core_form_alter(&$form, $form_state, $form_id) {
  // Debug code; display $form_id.
  if (module_exists('devel') && isset($_GET['debug']) && ($_GET['debug'] == 1 || isset($_GET['debug']['form_id']))) {
    dpm($form_id, '$form_id');
  }

  drupal_alter('beinghuman_core_form', $form, $form_state, $form_id);
  drupal_alter("beinghuman_core_form_{$form_id}", $form, $form_state);
}
