<?php
/**
 * @file
 * User module integration.
 */

/**
 * Implements hook_menu_alter().
 */
function beinghuman_core_menu_alter(&$items) {
  $items['user/%user']['access callback'] = 'beinghuman_core_user_view_access';
}

/**
 * Access callback for User profile page.
 */
function beinghuman_core_user_view_access($account) {
  $uid = is_object($account) ? $account->uid : (int) $account;

  // Never allow access to view the anonymous user account.
  if ($uid) {
    // Admins can view all, users can view own profiles at all times.
    if ($GLOBALS['user']->uid == $uid || user_access('administer users')) {
      return TRUE;
    }
    elseif ($GLOBALS['user']->uid == 0) {
      // At this point, load the complete account object.
      if (!is_object($account)) {
        $account = user_load($uid);
      }
      if (in_array('staff', $account->roles)) {
        return TRUE;
      }
    }
    elseif (user_access('access user profiles')) {
      // At this point, load the complete account object.
      if (!is_object($account)) {
        $account = user_load($uid);
      }
      return (is_object($account) && $account->status);
    }
  }
  return FALSE;
}

/**
 * Implements hook_entity_info_metadata_alter().
 */
function beinghuman_core_entity_info_metadata_alter(&$entity_info) {
  $entity_info['user']['access callback'] = 'beinghuman_core_user_entity_access';
}

/**
 * Access callback for User entity.
 *
 * @param      $op
 * @param null $entity
 * @param null $account
 * @param      $entity_type
 *
 * @return bool
 */
function beinghuman_core_user_entity_access($op, $entity = NULL, $account = NULL, $entity_type) {
  if (in_array('staff', $entity->roles)) {
    return TRUE;
  }

  return entity_metadata_user_access($op, $entity, $account, $entity_type);
}
