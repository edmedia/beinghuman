<?php
/**
 * @file
 * Basic Cart module integration.
 */

/**
 * Implements hook_preprocess_basic_cart_cart_render_block().
 */
function beinghuman_core_preprocess_basic_cart_cart_render_block(&$vars) {
  $vars['price'] = 0;
  foreach ($vars['cart'] as &$node) {
    $price_node = node_load($node->nid);
    $prices = beinghuman_core_get_prices($price_node);
    foreach ($prices as $price) {
      if ($node->basic_cart_quantity >= $price['min'] && (!isset($price['max']) || $node->basic_cart_quantity <= $price['max'])) {
        $node->basic_cart_unit_price = $price['price'];
      }
    }

    if (isset($node->basic_cart_quantity) && isset($node->basic_cart_unit_price)) {
      $vars['price'] += $node->basic_cart_quantity * $node->basic_cart_unit_price;
    }
  }
}
