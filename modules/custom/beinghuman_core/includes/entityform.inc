<?php
/**
 * @file
 * Entityforms module integration.
 */


/**
 * Implements hook_form_contact_us_entityform_edit_form_alter().
 */
function beinghuman_core_form_contact_us_entityform_edit_form_alter(&$form, $form_state) {
  $form['field_join_our_change_community_'][LANGUAGE_NONE]['#title_display'] = 'invisible';
}

/**
 * Implements hook_form_free_events_registration_entityform_edit_form_alter().
 */
function beinghuman_core_form_free_events_registration_entityform_edit_form_alter(&$form, $form_state) {

  $form['field_event_type_free'][LANGUAGE_NONE]['#ajax'] = array(
    'wrapper' => 'date-wrapper',
    'callback' => 'beinghuman_core_free_events_registration_entityform_edit_form_ajax',
  );

  $form['field_events_date']['#prefix'] = '<div id="date-wrapper">' . $form['field_events_date']['#prefix'];
  $form['field_events_date']['#suffix'] .= '</div>';

  $nids = !empty($form_state['values'])
    ? $form_state['values']['field_event_type_free'][LANGUAGE_NONE]
    : $form['field_event_type_free'][LANGUAGE_NONE]['#default_value'];
  if (!empty($nids)) {
    $args = array();
    foreach ($nids as $nid) {
      if (is_array($nid)) {
        $args[] = $nid['target_id'];
      }
      if (is_numeric($nid)) {
        $args[] = $nid;
      }
    }

    // Dates.
    $field = field_info_field('field_events_date');
    $field['settings']['handler_settings']['view']['args'][] = implode(',', $args);

    $instance = field_info_instance('entityform', $field['field_name'], 'free_events_registration');

    $element = array();
    $element = options_field_widget_form($form, $form_state, $field, $instance, LANGUAGE_NONE, $items, 0, $element);

    unset($element['#options']['_none']);

    $form['field_events_date'][LANGUAGE_NONE]['#options'] = $element['#options'];
    if (empty($element['#options'])) {
      $form['field_events_date']['#disabled'] = TRUE;
    }
  }
  $form['field_events_date'][LANGUAGE_NONE]['#multiple'] = TRUE;

  // Make the address field required because of salesforce
  $form['field_contact_address'][LANGUAGE_NONE][0]['street_block']['thoroughfare']['#required'] = TRUE;
}

/**
 * Implements hook_form_free_events_registration_entityform_edit_form_alter().
 */
function beinghuman_core_form_program_registration_entityform_edit_form_alter(&$form, $form_state) {
  $form['field_event_type_public'][LANGUAGE_NONE]['#ajax'] = array(
    'wrapper' => 'location-wrapper',
    'callback' => 'beinghuman_core_program_registration_entityform_edit_form_ajax',
  );
  $form['field_event_type_public'][LANGUAGE_NONE]['#limit_validation_errors'] =

  $form['field_event_location_public']['#prefix'] = '<div id="location-wrapper">' . $form['field_events_date']['#prefix'];
  $form['field_cost']['#suffix'] .= '</div>';

  $nid = !empty($form_state['values'])
    ? $form_state['values']['field_event_type_public'][LANGUAGE_NONE]
    : $form['field_event_type_public'][LANGUAGE_NONE]['#default_value'][0];
  if (!empty($nid)) {
    // Locations.
    $field = field_info_field('field_event_location_public');
    $field['settings']['handler_settings']['view']['args'][] = $nid;

    $instance = field_info_instance('entityform', $field['field_name'], 'program_registration');

    $element = array();
    $element = options_field_widget_form($form, $form_state, $field, $instance, LANGUAGE_NONE, $items, 0, $element);

    $form['field_event_location_public'][LANGUAGE_NONE]['#options'] = $element['#options'];

    // Cost.
    $node = node_load($nid);
    $items = array(
      'field_service_cost_early_bird' => t('Early bird'),
      'field_service_cost_single' => t('Single delegate'),
      'field_service_cost_team' => t('Project Team Discount'),
      'field_service_cost_cmi_acmp' => t('CMI/ACMP Discount'),
    );
    foreach ($items as $field_name => $title) {
      if (isset($node->{$field_name}[LANGUAGE_NONE][0]['value'])) {
        $value = t('@title - AUD $@cost', array(
          '@title' => $title,
          '@cost' => $node->{$field_name}[LANGUAGE_NONE][0]['value']));

        $form['field_cost'][LANGUAGE_NONE]['#options'][$value] = $value;
      }
    }
  }
}

/**
 * Ajax callback for 'Free Events Registration' form 'Type' field.
 */
function beinghuman_core_free_events_registration_entityform_edit_form_ajax($form, $form_state) {
  return $form['field_events_date'];
}

/**
 * Ajax callback for 'Program Registration' form 'Type' field.
 */
function beinghuman_core_program_registration_entityform_edit_form_ajax($form, $form_state) {
  return array($form['field_event_location_public'], $form['field_cost']);
}

/**
 * Implements hook_entity_insert().
 */
function beinghuman_core_entity_insert($entity, $type) {
  if ($type == 'entityform' && isset($entity->field_billing_details)) {
    if ($entity->field_billing_details[LANGUAGE_NONE][0]['value'] == t('As Above')) {
      $entity->field_billing_name[LANGUAGE_NONE][0]['value'] = "{$entity->field_first_name[LANGUAGE_NONE][0]['value']} {$entity->field_last_name[LANGUAGE_NONE][0]['value']}";
      $entity->field_billling_organisation = $entity->field_organisation;
      $entity->field_billing_address = $entity->field_contact_address;
    }
  }
}
