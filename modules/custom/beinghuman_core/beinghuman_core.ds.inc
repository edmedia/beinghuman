<?php
/**
 * @file
 * beinghuman_core.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function beinghuman_core_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|product|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'product';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'price' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => '1-29 copies Price(inc. GST)',
        ),
      ),
    ),
    'field_second_price' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => '30-99 copies Price(inc. GST)',
        ),
      ),
    ),
    'field_third_price' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => '100 or more Price(inc. GST)',
        ),
      ),
    ),
  );
  $export['node|product|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|product|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'product';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h2',
        'class' => '',
        'ft' => array(),
      ),
    ),
    'node_link' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'price' => array(
      'formatter_settings' => array(
        'ft' => array(
          'lb' => 'Price',
        ),
      ),
    ),
  );
  $export['node|product|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function beinghuman_core_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|product|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'product';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'uc_product_image',
        1 => 'field_product_prices',
        2 => 'add_to_cart',
        3 => 'body',
        4 => 'taxonomy_catalog',
      ),
    ),
    'fields' => array(
      'uc_product_image' => 'ds_content',
      'field_product_prices' => 'ds_content',
      'add_to_cart' => 'ds_content',
      'body' => 'ds_content',
      'taxonomy_catalog' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|product|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|product|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'product';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'uc_product_image',
        2 => 'body',
        3 => 'price',
        4 => 'add_to_cart',
        5 => 'node_link',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'uc_product_image' => 'ds_content',
      'body' => 'ds_content',
      'price' => 'ds_content',
      'add_to_cart' => 'ds_content',
      'node_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|product|teaser'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'taxonomy_term|service_location|full';
  $ds_layout->entity_type = 'taxonomy_term';
  $ds_layout->bundle = 'service_location';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'title',
        1 => 'field_location_image',
      ),
      'left' => array(
        2 => 'description',
        3 => 'field_location_address',
        4 => 'field_location_phone',
        5 => 'field_email_address',
        6 => 'field_location_website',
        7 => 'field_location_pdf',
      ),
    ),
    'fields' => array(
      'title' => 'header',
      'field_location_image' => 'header',
      'description' => 'left',
      'field_location_address' => 'left',
      'field_location_phone' => 'left',
      'field_email_address' => 'left',
      'field_location_website' => 'left',
      'field_location_pdf' => 'left',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['taxonomy_term|service_location|full'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'user|user|default';
  $ds_layout->entity_type = 'user';
  $ds_layout->bundle = 'user';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_user_position',
        1 => 'field_user_image',
        2 => 'field_user_pdf',
        3 => 'field_user_biography',
      ),
    ),
    'fields' => array(
      'field_user_position' => 'ds_content',
      'field_user_image' => 'ds_content',
      'field_user_pdf' => 'ds_content',
      'field_user_biography' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['user|user|default'] = $ds_layout;

  return $export;
}
