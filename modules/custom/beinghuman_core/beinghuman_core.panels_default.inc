<?php
/**
 * @file
 * beinghuman_core.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function beinghuman_core_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'footer';
  $mini->category = '';
  $mini->admin_title = 'Footer';
  $mini->admin_description = 'Footer section of the website';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-1';
    $pane->panel = 'center';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Subscribe',
      'title' => 'Subscribe',
      'body' => '<p>
Join our Change Community.
<br>
Membership is FREE.
</p>
<form id="subscribe-form" method="POST" action="/resources/change-community">
<input id="subscribe-email" type="text" placeholder="Your email address" name="email">
<input id="subscribe-submit" type="submit" value="SEND">
</form>
<p>
<b>Contact Us</b>
<br>
<span>(02) 9810 6264</span>
<br>
<span>International +61 2 9810 6264</span>
<br>
<a href="mailto:info@beinghuman.com.au">info@beinghuman.com.au</a>
</p>
<div class="social-icons">
<a class="linkedin" target="_blank" href="http://www.linkedin.com/company/being-human-pty-ltd?trk=hb_tab_compy_id_749447">LinkedIn</a>
<a class="twitter" target="_blank" href="http://twitter.com/BeingHuman_AU">Twitter</a>
<a class="facebook" target="_blank" href="https://www.facebook.com/pages/Being-Human-Pty-Ltd/487638891321917?fref=ts">Facebook</a>
</div>',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'footer-subscribe',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $display->content['new-1'] = $pane;
    $display->panels['center'][0] = 'new-1';
    $pane = new stdClass();
    $pane->pid = 'new-2';
    $pane->panel = 'center';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Endorsements',
      'title' => 'Endorsed by',
      'body' => '<div class="prosci-content">
<img alt="Prosci Global Affiliate Network" src="/profiles/beinghuman/themes/custom/beinghuman_theme/images/Prosci-Logo.png">
<p>
Prosci® is the world’s leading Change Management research and publishing organisation, founded in 1994. Its ADKAR® model, Change Management Methodology and training programs are used by over 3,500 organisations worldwide.
<a href="/about-prosci">Read more</a>
</p>
</div>
<ul>
<li>
<a href="/services/category/Free_Events/Prosci_Webinars">Prosci Webinars</a>
</li>
<li>
<a href="/about-prosci/prosci-methodology">Prosci Methodology</a>
</li>
<li>
<a href="/about-prosci">About Prosci</a>
</li>
<li>
<a href="/about-prosci/enterprise-change-management">Enterprise Change Management</a>
</li>
<li>
<a href="/about-prosci/best-practices-research">Best practices research</a>
</li>
<li>
<a href="/about-prosci/adkar-model">ADKAR model</a>
</li>
</ul>
<a href="https://www.change-management-institute.com/endorsed-courses"><img src="/profiles/beinghuman/themes/custom/beinghuman_theme/images/Endorsed_CMI.png"></a>
<a href="http://www.pmi.org/Professional-Development/REP-Find-a-Registered-Education-Provider.aspx"><img src="/profiles/beinghuman/themes/custom/beinghuman_theme/images/PMI_logo.png"></a>
<a href="http://www.aipm.com.au/AIPM/COURSES/ENDORSED_COURSES/AIPM/Lists/TPs_List.aspx?hkey=804531d2-a72b-452a-a1b3-fca089d5fcf1"><img src="/profiles/beinghuman/themes/custom/beinghuman_theme/images/aiopm.png"></a>',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'footer-endorsements',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $display->content['new-2'] = $pane;
    $display->panels['center'][1] = 'new-2';
    $pane = new stdClass();
    $pane->pid = 'new-3';
    $pane->panel = 'center';
    $pane->type = 'views';
    $pane->subtype = 'latest_blog';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '5',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 1,
      'override_title_text' => 'From our Blog',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'footer-blog',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $display->content['new-3'] = $pane;
    $display->panels['center'][2] = 'new-3';
    $pane = new stdClass();
    $pane->pid = 'new-4';
    $pane->panel = 'center';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Copyright',
      'title' => '',
      'body' => '<p>
2013 Being Human | <a href="/contact-us">Contact</a> | <a href="/sitemap">Site Map</a> | <a href="/privacy">Privacy Policy</a>
</p>',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'footer-copyright',
    );
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $display->content['new-4'] = $pane;
    $display->panels['center'][3] = 'new-4';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['footer'] = $mini;

  return $export;
}
