<?php
/**
 * @file
 * beinghuman_core.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function beinghuman_core_taxonomy_default_vocabularies() {
  return array(
    'blog_tag' => array(
      'name' => 'Blog tag',
      'machine_name' => 'blog_tag',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'catalog' => array(
      'name' => 'Bookstore categories',
      'machine_name' => 'catalog',
      'description' => 'Bookstore categories',
      'hierarchy' => 0,
      'module' => 'uc_catalog',
      'weight' => 0,
    ),
  );
}
