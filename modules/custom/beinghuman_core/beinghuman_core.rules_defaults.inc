<?php
/**
 * @file
 * beinghuman_core.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function beinghuman_core_default_rules_configuration() {
  $items = array();
  $items['beinghuman_core_webtolead_checkout'] = entity_import('rules_config', '{ "beinghuman_core_webtolead_checkout" : {
      "LABEL" : "WebToLead: Checkout",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "rules_http_client", "basic_cart", "entityform" ],
      "ON" : { "entityform_insert" : [] },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "entityform" ],
            "type" : "entityform",
            "bundle" : { "value" : { "checkout" : "checkout" } }
          }
        }
      ],
      "DO" : [
        { "request_url" : {
            "USING" : {
              "url" : "https:\\/\\/www.salesforce.com\\/servlet\\/servlet.WebToLead?encoding=UTF-8",
              "headers" : "Content-Type: application\\/x-www-form-urlencoded",
              "method" : "POST",
              "data" : "email=\\u0022[entityform:field-email-address]\\u0022\\r\\nstate=\\u0022[entityform:field-contact-address:administrative_area]\\u0022\\r\\ntitle=\\u0022[entityform:field-job-title]\\u0022\\r\\n00ND0000003TlBL=\\u0022[entityform:field-billing-address:postal_code]\\u0022\\r\\n00ND0000003TlBB=\\u0022[entityform:field-billing-name]\\u0022\\r\\n00ND0000003TlBG=\\u0022[entityform:field-billing-address:thoroughfare]\\u0022\\r\\noid=00D20000000J96b\\r\\n00N20000002JeI8=\\u0022[entityform:field-card-type]\\u0022\\r\\n00ND0000003TlBR=\\u0022[entityform:field-billing-address:country]\\u0022\\r\\n00ND0000003TlBQ=\\u0022[entityform:field-billing-address:administrative_area]\\u0022\\r\\nlast_name=\\u0022[entityform:field-last-name]\\u0022\\r\\n00ND0000003Tsse=\\u0022[entityform:field-billing-address:locality]\\u0022\\r\\nphone=\\u0022[entityform:field_phone]\\u0022\\r\\nstreet=\\u0022[entityform:field-contact-address:thoroughfare]\\u0022\\r\\ncompany=\\u0022[entityform:field-organisation]\\u0022\\r\\ncountry=\\u0022[entityform:field-contact-address:country]\\u0022\\r\\n00ND0000003TlAT=\\u0022[entityform:field-billling-organisation]\\u0022\\r\\nretURL=https:\\/\\/beinghuman.com.au\\/thank-you\\r\\n00N20000001iJkb=Bookstore\\r\\ncity=\\u0022[entityform:field-contact-address:locality]\\u0022\\r\\n00ND0000005XWhI=\\u0022[entityform:field-cart]\\u0022\\r\\n00N20000002JeII=\\u0022[entityform:field-card-expiry]\\u0022\\r\\n00N20000002JeIN=\\u0022[entityform:field-cardholders-name]\\u0022\\r\\n00ND0000005XWhN=\\u0022[entityform:field-total]\\u0022\\r\\n00ND00000053Fem=1\\r\\n00N20000002JeID=\\u0022[entityform:field-payment-method]\\u0022\\r\\n00N20000002JeIX=\\u0022[entityform:field-3-or-4-digit-ccv]\\u0022\\r\\nfirst_name=\\u0022[entityform:field-first-name]\\u0022\\r\\n00N20000002JePU=\\u0022[entityform:field-contact-address:postal_code]\\u0022\\r\\n00N20000002JeIS=\\u0022[entityform:field-card-number]\\u0022\\r\\n00ND0000003TlB6=\\u0022[entityform:field-billing-details]\\u0022"
            },
            "PROVIDE" : { "http_response" : { "http_response" : "HTTP data" } }
          }
        },
        { "component_rules_clean_secure_data" : { "entityform" : [ "entityform" ] } },
        { "basic_cart_empty_cart" : [] }
      ]
    }
  }');
  $items['beinghuman_core_webtolead_program_registration'] = entity_import('rules_config', '{ "beinghuman_core_webtolead_program_registration" : {
      "LABEL" : "WebToLead: Program Registration",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "rules_http_client", "entityform" ],
      "ON" : { "entityform_insert" : [] },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "entityform" ],
            "type" : "entityform",
            "bundle" : { "value" : { "program_registration" : "program_registration" } }
          }
        }
      ],
      "DO" : [
        { "request_url" : {
            "USING" : {
              "url" : "https:\\/\\/www.salesforce.com\\/servlet\\/servlet.WebToLead?encoding=UTF-8",
              "headers" : "Content-Type: application\\/x-www-form-urlencoded",
              "method" : "POST",
              "data" : "servicename=\\u0022[entityform:field-event-type-public]\\u0022\\r\\n00N20000002JeOR=\\u0022[entityform:field-event-location-public]\\u0022\\r\\n00N20000002JeIY=\\u0022[entityform:field-cost]\\u0022\\r\\n00N20000001iJkb=\\u0022[entityform:field-event-type-public:field-service-salesforce-code]\\u0022\\r\\nddlNumParticipants=\\u0022[entityform:field-number-of-participants]\\u0022\\r\\nfirst_name=\\u0022[entityform:field-first-name]\\u0022\\r\\nlast_name=\\u0022[entityform:field-last-name]\\u0022\\r\\ntitle=\\u0022[entityform:field-job-title]\\u0022\\r\\ncompany=\\u0022[entityform:field-organisation]\\u0022\\r\\nstreet=\\u0022[entityform:field-contact-address:thoroughfare]\\u0022\\r\\ncity=\\u0022[entityform:field-contact-address:locality]\\u0022\\r\\n00N20000002JePU=\\u0022[entityform:field-contact-address:postal_code]\\u0022\\r\\nstate=\\u0022[entityform:field-contact-address:administrative_area]\\u0022\\r\\ncountry=\\u0022[entityform:field-contact-address:country]\\u0022\\r\\nphone=\\u0022[entityform:field-phone]\\u0022\\r\\nemail=\\u0022[entityform:field-email-address]\\u0022\\r\\n00N20000002JeNi=\\u0022[entityform:field-emergency-contact-name]\\u0022\\r\\n00N20000002JeNe=\\u0022[entityform:field-emergency-contact-number]\\u0022\\r\\n00N20000001iJZG=\\u0022[entityform:field-dietary-requirements]\\u0022\\r\\n00N20000001iJZ3=\\u0022[entityform:field-additional-information]\\u0022\\r\\n00N20000002JeNJ=\\u0022[entityform:field-p2-first-name]\\u0022\\r\\n00N20000002JeNO=\\u0022[entityform:field-p2-last-name]\\u0022\\r\\n00N20000002JeNT=\\u0022[entityform:field-p2-job-title]\\u0022\\r\\n00N20000002JeNY=\\u0022[entityform:field-p2-phone-number]\\u0022\\r\\n00N20000002JeNd=\\u0022[entityform:field-p2-email-address]\\u0022\\r\\n00N20000002JeOH=\\u0022[entityform:field-p2-emergency-contact-name]\\u0022\\r\\n00N20000002JeOM=\\u0022[entityform:field-p2-emergency-contact-numbe]\\u0022\\r\\n00N20000002JeNn=\\u0022[entityform:field-p3-first-name]\\u0022\\r\\n00N20000002JeNs=\\u0022[entityform:field-p3-last-name]\\u0022\\r\\n00N20000002JeNx=\\u0022[entityform:field-p3-job-title]\\u0022\\r\\n00N20000002JeO2=\\u0022[entityform:field-p3-phone-number]\\u0022\\r\\n00N20000002JeNK=\\u0022[entityform:field-p3-email-address]\\u0022\\r\\n00N20000002JeO7=\\u0022[entityform:field-p3-emergency-contact-name]\\u0022\\r\\n00N20000002JeOC=\\u0022[entityform:field-p3-emergency-contact-numbe]\\u0022\\r\\n00N20000002JeID=\\u0022[entityform:field-payment-method]\\u0022\\r\\n00N20000002JeIc=\\u0022[entityform:field-promotional-code]\\u0022\\r\\n00N20000002JeI8=\\u0022[entityform:field-card-type]\\u0022\\r\\n00N20000002JeIN=\\u0022[entityform:field-cardholders-name]\\u0022\\r\\n00N20000002JeIS=\\u0022[entityform:field-card-number]\\u0022\\r\\n00N20000002JeII=\\u0022[entityform:field-card-expiry]\\u0022\\r\\n00N20000002JeIX=\\u0022[entityform:field-3-or-4-digit-ccv]\\u0022\\r\\n00ND0000003TlB6=\\u0022[entityform:field-billing-details]\\u0022\\r\\n00ND0000003TlBB=\\u0022[entityform:field-billing-name]\\u0022\\r\\n00ND0000003TlAT=\\u0022[entityform:field-billling-organisation]\\u0022\\r\\n00ND0000003TlBG=\\u0022[entityform:field-billing-address:thoroughfare]\\u0022\\r\\n00ND0000003Tsse=\\u0022[entityform:field-billing-address:locality]\\u0022\\r\\n00ND0000003TlBL=\\u0022[entityform:field-billing-address:postal_code]\\u0022\\r\\n00ND0000003TlBQ=\\u0022[entityform:field-billing-address:administrative_area]\\u0022\\r\\n00ND0000003TlBR=\\u0022[entityform:field-billing-address:country]\\u0022\\r\\nretURL=http:\\/\\/beinghuman.com.au\\/thank-you\\r\\noid=00D20000000J96b\\r\\n00ND00000053Fem=1\\r\\n00ND00000053Fer=\\u0022[entityform:created:custom:d\\/m\\/Y]\\u0022\\r\\n00ND00000053Few=1\\r\\n00ND00000053Ff1=\\u0022[entityform:created:custom:d\\/m\\/Y]\\u0022"
            },
            "PROVIDE" : { "http_response" : { "http_response" : "HTTP data" } }
          }
        },
        { "component_rules_clean_secure_data" : { "entityform" : [ "entityform" ] } }
      ]
    }
  }');
  $items['beinghuman_core_webtolead_webinar_registration'] = entity_import('rules_config', '{ "beinghuman_core_webtolead_webinar_registration" : {
      "LABEL" : "WebToLead: Free Events Registration",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "rules_http_client", "entityform" ],
      "ON" : { "entityform_insert" : [] },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "entityform" ],
            "type" : "entityform",
            "bundle" : { "value" : { "free_events_registration" : "free_events_registration" } }
          }
        }
      ],
      "DO" : [
        { "variable_add" : {
            "USING" : {
              "type" : "text",
              "value" : "email=\\u0022[entityform:field-email-address]\\u0022\\r\\nstate=\\u0022[entityform:field-contact-address:administrative_area]\\u0022\\r\\ntitle=\\u0022[entityform:field-job-title]\\u0022\\r\\ncompany=\\u0022[entityform:field-organisation]\\u0022\\r\\noid=00D20000000J96b\\r\\ncountry=\\u0022[entityform:field-contact-address:country]\\u0022\\r\\nwebinarservice=\\u0022[entityform:field-event-type-free]\\u0022\\r\\nretURL=https:\\/\\/beinghuman.com.au\\/thank-you\\r\\n00N20000001iJkb=\\u0022[entityform:field-event-type-free:field-service-salesforce-code]\\u0022\\r\\ncity=\\u0022[entityform:field-contact-address:locality]\\u0022\\r\\nlast_name=\\u0022[entityform:field-last-name]\\u0022\\r\\n00ND00000053Fem=1\\r\\nphone=\\u0022[entityform:field-phone]\\u0022\\r\\n00N20000001iGvy=\\u0022[entityform:field-additional-comments]\\u0022\\r\\nfirst_name=\\u0022[entityform:field-first-name]\\u0022\\r\\nDescription=\\u0022[entityform:created:custom:d\\/m\\/Y]\\u0022\\r\\n00ND00000053Fer=\\u0022[entityform:created:custom:d\\/m\\/Y]\\u0022\\r\\n00ND00000053Few=1\\r\\n00N20000002JePU=[entityform:field-contact-address:postal_code]\\r\\nzip=[entityform:field-contact-address:postal_code]\\r\\nstreet=[entityform:field-contact-address:thoroughfare]"
            },
            "PROVIDE" : { "variable_added" : { "data" : "Data" } }
          }
        },
        { "LOOP" : {
            "USING" : { "list" : [ "entityform:field-events-date" ] },
            "ITEM" : { "date" : "Date" },
            "DO" : [
              { "data_set" : {
                  "data" : [ "data" ],
                  "value" : "[data:value]\\r\\n00ND0000004DOl3[]=\\u0022[date:title]\\u0022"
                }
              }
            ]
          }
        },
        { "request_url" : {
            "USING" : {
              "url" : "https:\\/\\/www.salesforce.com\\/servlet\\/servlet.WebToLead?encoding=UTF-8",
              "headers" : "Content-Type: application\\/x-www-form-urlencoded",
              "method" : "POST",
              "data" : [ "data" ]
            },
            "PROVIDE" : { "http_response" : { "http_response" : "HTTP data" } }
          }
        }
      ]
    }
  }');
  $items['rules_clean_secure_data'] = entity_import('rules_config', '{ "rules_clean_secure_data" : {
      "LABEL" : "Clean secure data",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "entityform" : { "label" : "Entity form", "type" : "entityform" } },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "entityform" ], "field" : "field_3_or_4_digit_ccv" } },
        { "entity_has_field" : { "entity" : [ "entityform" ], "field" : "field_card_number" } }
      ],
      "DO" : [ { "entity_delete" : { "data" : [ "entityform" ] } } ]
    }
  }');
  $items['rules_webtolead_contact_us'] = entity_import('rules_config', '{ "rules_webtolead_contact_us" : {
      "LABEL" : "WebToLead: Contact Us",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "rules_http_client", "entityform" ],
      "ON" : { "entityform_insert" : [] },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "entityform" ],
            "type" : "entityform",
            "bundle" : { "value" : { "contact_us" : "contact_us" } }
          }
        }
      ],
      "DO" : [
        { "request_url" : {
            "USING" : {
              "url" : "https:\\/\\/www.salesforce.com\\/servlet\\/servlet.WebToLead?encoding=UTF-8",
              "headers" : "Content-Type: application\\/x-www-form-urlencoded",
              "method" : "POST",
              "data" : "oid=00D20000000J96b\\r\\nretURL=https:\\/\\/beinghuman.com.au\\/thank-you\\r\\n00N20000001iJkb=info\\r\\nfirst_name=\\u0022[entityform:field-first-name]\\u0022\\r\\nlast_name=\\u0022[entityform:field-last-name]\\u0022\\r\\ntitle=\\u0022[entityform:field-job-title]\\u0022\\r\\ncompany=\\u0022[entityform:field-organisation]\\u0022\\r\\nphone=\\u0022[entityform:field-phone]\\u0022\\r\\nemail=\\u0022[entityform:field-email-address]\\u0022\\r\\ncity=\\u0022[entityform:field-contact-address:locality]\\u0022\\r\\nstate=\\u0022[entityform:field-contact-address:administrative_area]\\u0022\\r\\n00N20000002JePU=\\u0022[entityform:field-contact-address:postal_code]\\u0022\\r\\ncountry=\\u0022[entityform:field-contact-address:country]\\u0022\\r\\n00ND00000053Fem=\\u0022[entityform:field_join_our_change_community_]\\u0022\\r\\nsubmit=Submit\\r\\n00N20000001iGvy=\\u0022[entityform:field-additional-comments]\\u0022"
            },
            "PROVIDE" : { "http_response" : { "http_response" : "HTTP data" } }
          }
        }
      ]
    }
  }');
  $items['rules_webtolead_join_community'] = entity_import('rules_config', '{ "rules_webtolead_join_community" : {
      "LABEL" : "WebToLead: Join community",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "rules_http_client", "entityform" ],
      "ON" : { "entityform_insert" : [] },
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "entityform" ],
            "type" : "entityform",
            "bundle" : { "value" : { "join_our_change_community" : "join_our_change_community" } }
          }
        }
      ],
      "DO" : [
        { "request_url" : {
            "USING" : {
              "url" : "https:\\/\\/www.salesforce.com\\/servlet\\/servlet.WebToLead?encoding=UTF-8",
              "headers" : "Content-Type: application\\/x-www-form-urlencoded",
              "method" : "POST",
              "data" : "oid=00D20000000J96b\\r\\nretURL=https:\\/\\/beinghuman.com.au\\/thank-you\\r\\n00N20000001iJkb=Contact\\r\\nfirst_name=\\u0022[entityform:field-first-name]\\u0022\\r\\nlast_name=\\u0022[entityform:field-last-name]\\u0022\\r\\ntitle=\\u0022[entityform:field-job-title]\\u0022\\r\\ncompany=\\u0022[entityform:field-organisation]\\u0022\\r\\ncity=\\u0022[entityform:field-contact-address:locality]\\u0022\\r\\nstate=\\u0022[entityform:field-contact-address:administrative_area]\\u0022\\r\\ncountry=\\u0022[entityform:field-contact-address:country]\\u0022\\r\\nphone=\\u0022[entityform:field-location-phone]\\u0022\\r\\nemail=\\u0022[entityform:field-email-address]\\u0022\\r\\n00ND00000053Fem=1\\r\\n00ND00000053Fer=\\u0022[entityform:created:custom:d\\/m\\/Y]\\u0022\\r\\n00ND00000053Few=1\\r\\n00ND00000053Ff1=\\u0022[entityform:created:custom:d\\/m\\/Y]\\u0022\\r\\nsubmit=Submit"
            },
            "PROVIDE" : { "http_response" : { "http_response" : "HTTP data" } }
          }
        }
      ]
    }
  }');
  return $items;
}
