<?php
/**
 * @file
 * beinghuman_core.multifield.inc
 */

/**
 * Implements hook_multifield_default_multifield().
 */
function beinghuman_core_multifield_default_multifield() {
  $export = array();

  $multifield = new stdClass();
  $multifield->disabled = FALSE; /* Edit this to true to make a default multifield disabled initially */
  $multifield->api_version = 1;
  $multifield->machine_name = 'prices';
  $multifield->label = 'Prices';
  $multifield->description = '';
  $export['prices'] = $multifield;

  return $export;
}
