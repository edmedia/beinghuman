<?php
/**
 * @file
 * beinghuman_core.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function beinghuman_core_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "bean_admin_ui" && $api == "bean") {
    return array("version" => "5");
  }
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "custom_formatters" && $api == "custom_formatters") {
    return array("version" => "2");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "menu_block" && $api == "menu_block") {
    return array("version" => "1");
  }
  if ($module == "multifield" && $api == "multifield") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function beinghuman_core_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_entityform_type().
 */
function beinghuman_core_default_entityform_type() {
  $items = array();
  $items['checkout'] = entity_import('entityform_type', '{
    "type" : "checkout",
    "label" : "Checkout",
    "data" : {
      "draftable" : 0,
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "plain_text" },
      "submit_button_text" : "",
      "submit_confirm_msg" : "",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "",
      "submission_text" : { "value" : "", "format" : "plain_text" },
      "submission_show_submitted" : 0,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : "2", "3" : 0, "4" : 0 },
      "resubmit_action" : "new",
      "redirect_path" : "",
      "instruction_pre" : {
        "value" : "\\u003Ch3\\u003EPostage Policy\\u003C\\/h3\\u003E\\u003Cp\\u003EThe following delivery charges will be added to the total value of your order:\\u003C\\/p\\u003E\\u003Cp\\u003EOrders under AUD$250:\\u003C\\/p\\u003E\\u003Cp\\u003EAustralia \\u0026amp; International: The cost of ordinary postage plus AUD $10.00 handling fee will be added to your order amount.\\u003C\\/p\\u003E\\u003Cp\\u003EOrders over AUD$250:\\u003C\\/p\\u003E\\u003Cp\\u003ESydney Metro: AUD$20.00 delivery fee by next day courier will be added to your order amount. NSW, Interstate and International: Australia Post Registered Mail charge plus AUD $10.00 handling fee will be added to your order amount.\\u003C\\/p\\u003E\\u003Cp\\u003EPlease contact us if you require Express Delivery.\\u003C\\/p\\u003E",
        "format" : "wysiwyg"
      }
    },
    "weight" : "0",
    "paths" : { "submit" : {
        "source" : "eform\\/submit\\/checkout",
        "alias" : "checkout",
        "language" : "und"
      }
    }
  }');
  $items['contact_us'] = entity_import('entityform_type', '{
    "type" : "contact_us",
    "label" : "Contact Us",
    "data" : {
      "draftable" : 0,
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "plain_text" },
      "submit_button_text" : "",
      "submit_confirm_msg" : "",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "",
      "submission_text" : { "value" : "", "format" : "plain_text" },
      "submission_show_submitted" : 0,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : "2", "3" : 0, "4" : 0, "5" : 0 },
      "resubmit_action" : "new",
      "redirect_path" : "",
      "instruction_pre" : {
        "value" : "\\u003Cp\\u003EBeing Human Pty Ltd\\u003Cbr \\/\\u003EPO Box 1507,\\u0026nbsp;Rozelle NSW 2039\\u003Cbr \\/\\u003EPhone +61 2 9810 6264\\u003Cbr \\/\\u003EEmail \\u003Ca href=\\u0022mailto:info@beinghuman.com.au\\u0022\\u003Einfo@beinghuman.com.au\\u003C\\/a\\u003E\\u003C\\/p\\u003E\\u003Cdiv\\u003E\\u003Cstrong\\u003E\\u003Cspan style=\\u0022color: rgb(0, 128, 128);\\u0022\\u003EFollow us on Facebook\\u003C\\/span\\u003E\\u003C\\/strong\\u003E\\u003Cdiv id=\\u0022fb-root\\u0022\\u003E\\u0026nbsp;\\u003C\\/div\\u003E\\u003Cscript\\u003E(function(d, s, id) {\\r\\n  var js, fjs = d.getElementsByTagName(s)[0];\\r\\n  if (d.getElementById(id)) return;\\r\\n  js = d.createElement(s); js.id = id;\\r\\n  js.src = \\u0022\\/\\/connect.facebook.net\\/en_GB\\/all.js#xfbml=1\\u0022;\\r\\n  fjs.parentNode.insertBefore(js, fjs);\\r\\n}(document, \\u0027script\\u0027, \\u0027facebook-jssdk\\u0027));\\u003C\\/script\\u003E\\u003Cdiv class=\\u0022fb-follow\\u0022 data-colorscheme=\\u0022dark\\u0022 data-href=\\u0022https:\\/\\/www.facebook.com\\/pages\\/Being-Human-Pty-Ltd\\/487638891321917?fref=ts\\u0022 data-layout=\\u0022button\\u0022 data-show-faces=\\u0022false\\u0022\\u003E\\u0026nbsp;\\u003C\\/div\\u003E\\u003Cp\\u003E\\u0026nbsp;\\u003C\\/p\\u003E\\u003C\\/div\\u003E\\u003Cp\\u003E\\u0026nbsp;\\u003C\\/p\\u003E",
        "format" : "full_html"
      }
    },
    "weight" : "0",
    "paths" : { "submit" : {
        "source" : "eform\\/submit\\/contact-us",
        "alias" : "contact-us",
        "language" : "und"
      }
    }
  }');
  $items['free_events_registration'] = entity_import('entityform_type', '{
    "type" : "free_events_registration",
    "label" : "Free Events Registration",
    "data" : {
      "draftable" : 0,
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "plain_text" },
      "submit_button_text" : "",
      "submit_confirm_msg" : "Thank you, your registration has been successfully sent.",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "",
      "submission_text" : { "value" : "", "format" : "plain_text" },
      "submission_show_submitted" : 0,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : "2", "3" : 0, "4" : 0, "5" : 0 },
      "resubmit_action" : "new",
      "redirect_path" : "",
      "instruction_pre" : { "value" : "", "format" : "plain_text" }
    },
    "weight" : "0",
    "paths" : { "submit" : {
        "source" : "eform\\/submit\\/free-events-registration",
        "alias" : "webinar-registration",
        "language" : "und"
      }
    }
  }');
  $items['join_our_change_community'] = entity_import('entityform_type', '{
    "type" : "join_our_change_community",
    "label" : "Join Our Change Community",
    "data" : {
      "draftable" : 0,
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "plain_text" },
      "submit_button_text" : "",
      "submit_confirm_msg" : "",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "",
      "submission_text" : { "value" : "", "format" : "plain_text" },
      "submission_show_submitted" : 0,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : "2", "3" : 0, "4" : 0, "5" : 0 },
      "resubmit_action" : "new",
      "redirect_path" : "",
      "instruction_pre" : {
        "value" : "\\u003Cdiv style=\\u0022float: left; width: 42%\\u0022\\u003E\\u003Cimg alt=\\u0022\\u0022 src=\\u0022\\/sites\\/beinghuman.com.au\\/files\\/people-holding-hands.jpg\\u0022 style=\\u0022width: 300px; height: 199px; float: right; padding: 10px;\\u0022 \\/\\u003E\\u003C\\/div\\u003E\\u003Cp\\u003EThere are no Lone Rangers in change.\\u003C\\/p\\u003E\\u003Cp\\u003EThe purpose of our Change Community is to support Change Managers and other specialists \\u0026nbsp;working in change, with professional development and networking opportunities to share experiences and workshop challenges.\\u0026nbsp;Membership is free.\\u003C\\/p\\u003E\\u003Cp\\u003EBy joining the Being Human Change Community, you will benefit from:\\u003C\\/p\\u003E\\u003Cul\\u003E\\u003Cli\\u003EChange Conversations e-newsletter \\u0026ndash; quarterly\\u003C\\/li\\u003E\\u003Cli\\u003EFree Change Conversations Seminars in selected cities\\u003C\\/li\\u003E\\u003Cli\\u003EInvitations to participate in Prosci\\u0026rsquo;s Change Management Webinars\\u003C\\/li\\u003E\\u003Cli\\u003EAdvance notice of special events and offers\\u003C\\/li\\u003E\\u003C\\/ul\\u003E\\u003Cdiv\\u003E\\u003Cstrong\\u003E\\u003Cspan style=\\u0022color: rgb(0, 128, 128);\\u0022\\u003EFollow us on Facebook\\u003C\\/span\\u003E\\u003C\\/strong\\u003E\\u003C\\/div\\u003E\\u003Cdiv id=\\u0022fb-root\\u0022\\u003E\\u0026nbsp;\\u003C\\/div\\u003E\\u003Cscript\\u003E(function(d, s, id) {\\r\\n  var js, fjs = d.getElementsByTagName(s)[0];\\r\\n  if (d.getElementById(id)) return;\\r\\n  js = d.createElement(s); js.id = id;\\r\\n  js.src = \\u0022\\/\\/connect.facebook.net\\/en_GB\\/all.js#xfbml=1\\u0022;\\r\\n  fjs.parentNode.insertBefore(js, fjs);\\r\\n}(document, \\u0027script\\u0027, \\u0027facebook-jssdk\\u0027));\\u003C\\/script\\u003E\\u003Cdiv class=\\u0022fb-follow\\u0022 data-colorscheme=\\u0022dark\\u0022 data-href=\\u0022https:\\/\\/www.facebook.com\\/pages\\/Being-Human-Pty-Ltd\\/487638891321917?fref=ts\\u0022 data-layout=\\u0022button\\u0022 data-show-faces=\\u0022false\\u0022\\u003E\\u0026nbsp;\\u003C\\/div\\u003E",
        "format" : "full_html"
      }
    },
    "weight" : "0",
    "paths" : { "submit" : {
        "source" : "eform\\/submit\\/join-our-change-community",
        "alias" : "resources\\/change-community",
        "language" : "und"
      }
    }
  }');
  $items['program_registration'] = entity_import('entityform_type', '{
    "type" : "program_registration",
    "label" : "Program Registration",
    "data" : {
      "draftable" : 0,
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "plain_text" },
      "submit_button_text" : "",
      "submit_confirm_msg" : "Thank you, your registration has been successfully sent.",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "",
      "submission_text" : { "value" : "", "format" : "plain_text" },
      "submission_show_submitted" : 0,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : "2", "3" : 0, "4" : 0, "5" : 0 },
      "resubmit_action" : "new",
      "redirect_path" : "",
      "instruction_pre" : {
        "value" : "\\u003Cp\\u003EYour place on the program will be confirmed when full payment is received. Being Human reserves the right to refuse a registration.\\u003Cbr \\/\\u003EA tax invoice will be sent to you when payment is received, together with the program joining information.\\u003C\\/p\\u003E\\u003Cp\\u003EIf you have any queries please call us on +61 2 9810 6264 or email us at \\u003Ca href=\\u0022mailto:info@beinghuman.com.au\\u0022\\u003Einfo@beinghuman.com.au\\u0026nbsp;\\u003C\\/a\\u003E\\u003C\\/p\\u003E",
        "format" : "wysiwyg"
      }
    },
    "weight" : "0",
    "paths" : { "submit" : {
        "source" : "eform\\/submit\\/program-registration",
        "alias" : "program-registration",
        "language" : "und"
      }
    }
  }');
  return $items;
}

/**
 * Implements hook_image_default_styles().
 */
function beinghuman_core_image_default_styles() {
  $styles = array();

  // Exported image style: team.
  $styles['team'] = array(
    'name' => 'team',
    'label' => 'Team',
    'effects' => array(
      1 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 139,
          'height' => 170,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function beinghuman_core_node_info() {
  $items = array(
    'faq' => array(
      'name' => t('FAQ'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Page'),
      'base' => 'node_content',
      'description' => t('Add a content page'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'product' => array(
      'name' => t('Product'),
      'base' => 'node_content',
      'description' => t('A product is a good or service that you wish to sell on your site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
