<?php
/**
 * @file
 * beinghuman_core.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function beinghuman_core_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'frontpage_block';
  $bean_type->label = 'Frontpage block';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['frontpage_block'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'frontpage_slide';
  $bean_type->label = 'Frontpage slide';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['frontpage_slide'] = $bean_type;

  return $export;
}
