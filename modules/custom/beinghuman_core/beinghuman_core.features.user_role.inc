<?php
/**
 * @file
 * beinghuman_core.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function beinghuman_core_user_default_roles() {
  $roles = array();

  // Exported role: editor.
  $roles['editor'] = array(
    'name' => 'editor',
    'weight' => 4,
  );

  // Exported role: staff.
  $roles['staff'] = array(
    'name' => 'staff',
    'weight' => 3,
  );

  return $roles;
}
