Reverting back to the pre-Realityloop codebase
--------------------------------------------------------------------------------

The initial commit into this repository contained this file and a stub.make
file.

The stub.make file contains all contrib modules and libraries, and references
all custom modules and patches/hacks from other branches of this repository.

In theory, you should be able to build the pre-Realityloop code base from the
initial version of the stub.make file, but unfortunately this is not always the
case as the core patch/hacks include a large amount of binary files and they
don't always seem to get created when Drush make is run.

In this case, the patch should be applied by hand.
