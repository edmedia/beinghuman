core = 7.x
api = 2



; Drupal core

projects[drupal][type] = core
projects[drupal][version] = 7.32
; Secure Pages fix for drupal_valid_tokens() - https://drupal.org/comment/8361131#comment-8361131
projects[drupal][patch][] = https://drupal.org/files/issues/961508-232.patch



; Profile

projects[beinghuman][type] = profile
projects[beinghuman][download][type] = git
projects[beinghuman][download][url] =https://chaloum@bitbucket.org/edmedia/beinghuman.git 
projects[beinghuman][download][branch] = 7.x-1.x
