<?php

/**
 * Implements hook_preprocess_page().
 */
function beinghuman_theme_preprocess_page(&$variables) {
  // You can use preprocess hooks to modify the variables before they are passed
  // to the theme function or template file.
  //
  if ($variables['page']['content']['system_main']['#entity_type'] == "user") {
  	$variables['page']['content']['system_main']['field_user_pdf'][0]['#file']->filename = "Download PDF";
  }
}
