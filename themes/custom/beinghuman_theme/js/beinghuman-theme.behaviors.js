(function ($) {

  /**
  */
  Drupal.behaviors.beinghumanThemeConditonalFieldsets = {
    attach: function (context, settings) {
      $('.page-eform-submit-program-registration', context).once('conditional-fieldsets', function () {
        $('#edit-field-number-of-participants-und').bind('change', function() {
          switch ($(this).val()) {
            case '1':
              $('#entityform_program_registration_form_group_participant2').hide();
              $('#entityform_program_registration_form_group_participant3').hide();
              break;

            case '2':
              $('#entityform_program_registration_form_group_participant2').show();
              $('#entityform_program_registration_form_group_participant3').hide();
              break;

            case '3':
              $('#entityform_program_registration_form_group_participant2').show();
              $('#entityform_program_registration_form_group_participant3').show();
              break;
          }
        }).trigger('change');;
      })
    }
  };

})(jQuery);
