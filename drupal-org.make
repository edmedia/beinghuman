core = 7.x
api = 2



; Modules

projects[addressfield][type] = module
projects[addressfield][subdir] = contrib
projects[addressfield][version] = 1.0-beta4

projects[administerusersbyrole][type] = module
projects[administerusersbyrole][subdir] = contrib
projects[administerusersbyrole][version] = 1.0-beta1

projects[admin_menu][type] = module
projects[admin_menu][subdir] = contrib
projects[admin_menu][version] = 3.0-rc4

projects[admin_select][type] = module
projects[admin_select][subdir] = contrib
projects[admin_select][version] = 1.3
; Navbar integration - https://drupal.org/files/issues/navbar-2085211-6.patch
projects[admin_select][patch][] = https://drupal.org/files/issues/navbar-2085211-6.patch

projects[adminimal_admin_menu][subdir] = contrib
projects[adminimal_admin_menu][download][type] = git
projects[adminimal_admin_menu][download][revision] = 5786a1f3088a82edd37378f81773ba07a673f1b0

projects[alchemy][type] = module
projects[alchemy][subdir] = contrib
projects[alchemy][version] = 1.0-beta1

projects[auto_nodetitle][type] = module
projects[auto_nodetitle][subdir] = contrib
projects[auto_nodetitle][version] = 1.0

projects[basic_cart][type] = module
projects[basic_cart][subdir] = contrib
projects[basic_cart][download][type] = git
projects[basic_cart][download][revision] = f81dd8c50eda72474080a14a8a8fdf7e1d03e687
; Add Quantity - https://drupal.org/node/1708206#comment-8222007
projects[basic_cart][patch][] = https://drupal.org/files/issues/quantity-1708206-7.patch
; Empty cart rules action - https://drupal.org/comment/8405913#comment-8405913
projects[basic_cart][patch][] = https://drupal.org/files/issues/rules-1930632-3.patch

projects[bean][type] = module
projects[bean][subdir] = contrib
projects[bean][version] = 1.7

projects[better_formats][type] = module
projects[better_formats][subdir] = contrib
projects[better_formats][version] = 1.0-beta1

projects[breakpoints][type] = module
projects[breakpoints][subdir] = contrib
projects[breakpoints][version] = 1.1

projects[calendar][type] = module
projects[calendar][subdir] = contrib
projects[calendar][version] = 3.4

projects[ckeditor][type] = module
projects[ckeditor][subdir] = contrib
projects[ckeditor][version] = 1.16

projects[coder][type] = module
projects[coder][subdir] = contrib
projects[coder][version] = 2.0

projects[coffee][type] = module
projects[coffee][subdir] = contrib
projects[coffee][version] = 2.0

projects[computed_field][type] = module
projects[computed_field][subdir] = contrib
projects[computed_field][version] = 1.0

projects[conditional_fields][type] = module
projects[conditional_fields][subdir] = contrib
projects[conditional_fields][version] = 3.0-alpha1

projects[contentanalysis][type] = module
projects[contentanalysis][subdir] = contrib
projects[contentanalysis][version] = 1.0-beta5

projects[contentoptimizer][type] = module
projects[contentoptimizer][subdir] = contrib
projects[contentoptimizer][version] = 2.0-beta4

projects[context][type] = module
projects[context][subdir] = contrib
projects[context][version] = 3.1

projects[context_condition_theme][type] = module
projects[context_condition_theme][subdir] = contrib
projects[context_condition_theme][version] = 1.0

projects[context_module][type] = module
projects[context_module][subdir] = contrib
projects[context_module][download][type] = git
projects[context_module][download][revision] = b7a05aa1299d9286b77f66c539826de9bebde2fb
; Prevent redirect loop - https://drupal.org/node/2076565#comment-7804715
projects[context_module][patch][] = https://drupal.org/files/redirect_loop-2076565-1.patch

projects[context_server_vars][type] = module
projects[context_server_vars][subdir] = contrib
projects[context_server_vars][download][type] = git
projects[context_server_vars][download][revision] = 1d61d973ddb7513de9c3d82b33c8e24c24e907f2

projects[context_variable][type] = module
projects[context_variable][subdir] = contrib
projects[context_variable][download][type] = git
projects[context_variable][download][revision] = cd9b3b9b64acd5ac3a1c6c180eadff34294c3f11

projects[crumbs][type] = module
projects[crumbs][subdir] = contrib
projects[crumbs][version] = 2.0-beta13

projects[ctools][type] = module
projects[ctools][subdir] = contrib
projects[ctools][version] = 1.4
; Fix for broken tokens - https://www.drupal.org/node/1624460#comment-8993685
projects[ctools][patch][] = https://www.drupal.org/files/issues/broken_token-1624460-14.patch

projects[custom_formatters][type] = module
projects[custom_formatters][subdir] = contrib
projects[custom_formatters][download][type] = git
projects[custom_formatters][download][revision] = bd4d6c3bbd90a830deb3972eed35a742eed347db

projects[date][type] = module
projects[date][subdir] = contrib
projects[date][version] = 2.6

projects[devel][type] = module
projects[devel][subdir] = contrib
projects[devel][version] = 1.3

projects[diff][type] = module
projects[diff][subdir] = contrib
projects[diff][version] = 3.2

projects[ds][type] = module
projects[ds][subdir] = contrib
projects[ds][version] = 2.6

projects[email][type] = module
projects[email][subdir] = contrib
projects[email][version] = 1.2

projects[entity][type] = module
projects[entity][subdir] = contrib
projects[entity][version] = 1.5
; Allow arguments for entity id - https://drupal.org/node/1957266#comment-8197605
projects[entity][patch][] = https://drupal.org/files/issues/entity_id_token-1957266-12.patch
; Allow altering of Entity's entity_info_alter() - https://drupal.org/node/2269989#comment-8791219
projects[entity][patch][] = https://drupal.org/files/issues/entity_info_metadata_alter-2269989-1.patch

projects[entitycache][type] = module
projects[entitycache][subdir] = contrib
projects[entitycache][version] = 1.1

projects[entityform][type] = module
projects[entityform][subdir] = contrib
projects[entityform][version] = 2.0-beta1
; Remove Field UI dependency - https://drupal.org/node/2123417#comment-8234417
projects[entityform][patch][] = https://drupal.org/files/issues/field_ui-2123417-3.patch

projects[entityreference][type] = module
projects[entityreference][subdir] = contrib
projects[entityreference][version] = 1.1

projects[environment_indicator][subdir] = contrib
projects[environment_indicator][download][type] = git
projects[environment_indicator][download][revision] = a88522bb914d829513a84750116a374a3dcd2f96

projects[features][type] = module
projects[features][subdir] = contrib
projects[features][version] = 2.0
; Cleaner info format - https://drupal.org/comment/8270327#comment-8270327
projects[features][patch][] = https://drupal.org/files/issues/cleaner_info-2155793-1.patch

projects[field_group][type] = module
projects[field_group][subdir] = contrib
projects[field_group][version] = 1.3

projects[google_analytics][type] = module
projects[google_analytics][subdir] = contrib
projects[google_analytics][version] = 1.4

projects[imce][type] = module
projects[imce][subdir] = contrib
projects[imce][version] = 1.7

projects[imce_wysiwyg][type] = module
projects[imce_wysiwyg][subdir] = contrib
projects[imce_wysiwyg][version] = 1.0

projects[jcarousel][type] = module
projects[jcarousel][subdir] = contrib
projects[jcarousel][version] = 2.6

projects[jplayer][type] = module
projects[jplayer][subdir] = contrib
projects[jplayer][version] = 2.0-beta1

projects[libraries][type] = module
projects[libraries][subdir] = contrib
projects[libraries][version] = 2.1

projects[link][type] = module
projects[link][subdir] = contrib
projects[link][version] = 1.2

projects[menu_block][type] = module
projects[menu_block][subdir] = contrib
projects[menu_block][download][type] = git
projects[menu_block][download][revision] = ba6140aa09cd92fd8394dffc854a1119f793dd5e
; Features exportable - https://drupal.org/comment/7946737#comment-7946737
projects[menu_block][patch][] = https://drupal.org/files/ctools_exportable-693302-119.patch

projects[metatag][type] = module
projects[metatag][subdir] = contrib
projects[metatag][version] = 1.0-beta7

projects[migrate][type] = module
projects[migrate][subdir] = contrib
projects[migrate][version] = 2.6-rc1

projects[module_filter][type] = module
projects[module_filter][subdir] = contrib
projects[module_filter][version] = 1.8

projects[multifield][type] = module
projects[multifield][subdir] = contrib
projects[multifield][version] = 1.0-unstable9

projects[navbar][type] = module
projects[navbar][subdir] = contrib
projects[navbar][download][type] = git
projects[navbar][download][revision] = 83e10263b59acf1f8067dbd4349b982bbf8eb4a0
; Add support for hook_suppress() - https://drupal.org/comment/8161199#comment-8161199
projects[navbar][patch][] = https://drupal.org/files/issues/navbar-hook_suppress-1728726-12.patch

projects[overlay_expand][type] = module
projects[overlay_expand][subdir] = contrib
projects[overlay_expand][version] = 1.0

projects[panels][type] = module
projects[panels][subdir] = contrib
projects[panels][version] = 3.3

projects[pathauto][type] = module
projects[pathauto][subdir] = contrib
projects[pathauto][version] = 1.2

projects[phone][type] = module
projects[phone][subdir] = contrib
projects[phone][download][type] = git
projects[phone][download][revision] = 173dd71fc755c61ed7757f3ea5d7e12ce970e5d9

projects[prepro][type] = module
projects[prepro][subdir] = contrib
projects[prepro][version] = 1.2
; Variable integration - http://drupal.org/node/1958564#comment-7269614
projects[prepro][patch][] = http://drupal.org/files/variable_integration-1958564-3.patch

projects[redirect][type] = module
projects[redirect][subdir] = contrib
projects[redirect][version] = 1.0-rc1

projects[reroute_email][type] = module
projects[reroute_email][subdir] = contrib
projects[reroute_email][version] = 1.1

projects[rules][type] = module
projects[rules][subdir] = contrib
projects[rules][version] = 2.6

projects[rules_http_client][type] = module
projects[rules_http_client][subdir] = contrib
projects[rules_http_client][download][type] = git
projects[rules_http_client][download][revision] = 8338a0b41fde5d347cb7040a84336fa7edf0d085

projects[sassy][type] = module
projects[sassy][subdir] = contrib
projects[sassy][version] = 2.13
; Fix squish-text mixin - http://drupal.org/node/1539310#comment-7284702
projects[sassy][patch][] = http://drupal.org/files/squish_text-1539310-5.patch

projects[search_krumo][type] = module
projects[search_krumo][subdir] = contrib
projects[search_krumo][version] = 1.5

projects[securepages][type] = module
projects[securepages][subdir] = contrib
projects[securepages][version] = 1.0-beta2
; Fix issue when all pages should redirect - https://drupal.org/node/566632#comment-8441723
projects[securepages][patch][] = https://drupal.org/files/issues/all_pages-566632-13.patch

projects[seotools][type] = module
projects[seotools][subdir] = contrib
projects[seotools][version] = 1.0-alpha5

projects[sidr_responsive_menu][type] = module
projects[sidr_responsive_menu][subdir] = contrib
projects[sidr_responsive_menu][download][type] = git
projects[sidr_responsive_menu][download][url] = http://git.drupal.org/sandbox/Swarad/2092357.git
projects[sidr_responsive_menu][download][revision] = f544309d6661b174ab07a213a7cf04af8b461e0f

projects[site_map][type] = module
projects[site_map][subdir] = contrib
projects[site_map][version] = 1.0

projects[strongarm][type] = module
projects[strongarm][subdir] = contrib
projects[strongarm][version] = 2.0

projects[styleguide][type] = module
projects[styleguide][subdir] = contrib
projects[styleguide][version] = 1.1

projects[taxonomy_display][type] = module
projects[taxonomy_display][subdir] = contrib
projects[taxonomy_display][version] = 1.1

projects[title][type] = module
projects[title][subdir] = contrib
projects[title][version] = 1.0-alpha7

projects[token][type] = module
projects[token][subdir] = contrib
projects[token][version] = 1.5

projects[variable][type] = module
projects[variable][subdir] = contrib
projects[variable][version] = 2.3

projects[views][type] = module
projects[views][subdir] = contrib
projects[views][version] = 3.8

projects[views_bulk_operations][type] = module
projects[views_bulk_operations][subdir] = contrib
projects[views_bulk_operations][version] = 3.1

projects[views_conditional][type] = module
projects[views_conditional][subdir] = contrib
projects[views_conditional][version] = 1.1

projects[views_limit_grouping][type] = module
projects[views_limit_grouping][subdir] = contrib
projects[views_limit_grouping][download][type] = git
projects[views_limit_grouping][download][revision] = a6466fce8da74783aacf31b9692c5ef78878e390

projects[views_slideshow][type] = module
projects[views_slideshow][subdir] = contrib
projects[views_slideshow][version] = 3.1

projects[wysiwyg][type] = module
projects[wysiwyg][subdir] = contrib
projects[wysiwyg][version] = 2.2

projects[wysiwyg_filter][type] = module
projects[wysiwyg_filter][subdir] = contrib
projects[wysiwyg_filter][version] = 1.6-rc2



; Themes

projects[omega][type] = theme
projects[omega][subdir] = contrib
projects[omega][version] = 4.1



; Libraries

libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.5/ckeditor_3.6.5.zip

libraries[html5shiv][download][type] = git
libraries[html5shiv][download][url] = https://github.com/fubhy/html5shiv.git
libraries[html5shiv][download][revision] = 31eb876324ce0636a7255647ae2116d413044ff6

libraries[jplayer][download][type] = get
libraries[jplayer][download][url] = http://jplayer.org/2.5.0/jQuery.jPlayer.2.5.0.zip

libraries[jquery.cycle][download][type] = git
libraries[jquery.cycle][download][url] = https://github.com/malsup/cycle.git
libraries[jquery.cycle][download][revision] = b1dc72bc44405523317e2a016972075a07437b16

libraries[json2][download][type] = git
libraries[json2][download][url] = https://github.com/douglascrockford/JSON-js.git
libraries[json2][download][revision] = ff55d8d4513b149e2511aee01c3a61d372837d1f

libraries[matchmedia][download][type] = git
libraries[matchmedia][download][url] = https://github.com/fubhy/matchmedia.git
libraries[matchmedia][download][revision] = d59a32e68a29a7061447e9528e2eb4b9deec8592

libraries[phpsass][download][type] = git
libraries[phpsass][download][url] = https://github.com/richthegeek/phpsass.git
libraries[phpsass][download][revision] = 38b33f638217a921c8abe00fbec372e62681a37d

libraries[pie][download][type] = git
libraries[pie][download][url] = https://github.com/fubhy/pie.git
libraries[pie][download][revision] = e68a7bd09ff1302754433e1493cbf9cee8c9aae1

libraries[respond][download][type] = git
libraries[respond][download][url] = https://github.com/fubhy/respond.git
libraries[respond][download][revision] = c8ffda96bbbbbd0a455a56b2067548f189235801

libraries[selectivizr][download][type] = git
libraries[selectivizr][download][url] = https://github.com/fubhy/selectivizr.git
libraries[selectivizr][download][revision] = 7db526a4934d22e7e8dcbd5906df0a7d603244a0


